# Postgres + Adminer 

Docker-compose contains -
* Postgres 15 
* Adminer 
* Wall-g for backup 

*NOTE -* Make sure that .gitkeep is deleted from the /data dir otherwise PG wont startup.

#### Tested Environment - Gitpod (ubuntu 22)

```
# docker-compose -f docker-compose.yaml up -d 
```

### Backup with Wal-g - 
Make sure that .env is created. The example .env is looks like below 

```
$ cat .env 

```
Take Full Backup
Take incremental Backup  
Restore Backup 